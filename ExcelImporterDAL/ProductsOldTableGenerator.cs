﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelImporterDAL
{
    public class ProductsOldTableGenerator : IDataTableGenerator
    {
        public DataTable GenerateDataTable(DataSet ds, int startRowNumber = 1)
        {
            DataTable table = new DataTable();
            table.Columns.Add("id", typeof(Int32));
   
            table.Columns.Add("is_1c", typeof(byte));
            table.Columns.Add("is_shop", typeof(byte));
            table.Columns.Add("category", typeof(string));
            table.Columns.Add("name", typeof(string));
            table.Columns.Add("reference", typeof(string));
            table.Columns.Add("unity", typeof(string));
            table.Columns.Add("manufacturer", typeof(string));
            table.Columns.Add("retail_price", typeof(decimal));
            table.Columns.Add("currency", typeof(string));
            table.Columns.Add("e_factor", typeof(decimal));
            table.Columns.Add("e_reduction", typeof(double));

            table.Columns.Add("m_factor_1", typeof(double));          
            table.Columns.Add("m_factor_2", typeof(double));          
            table.Columns.Add("m_factor_3", typeof(double));
           
            table.Columns.Add("date_updated", typeof(DateTime));

            foreach (DataTable dt in ds.Tables)
            {
                for (int i = startRowNumber; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    DataRow row = table.NewRow();

                    row["id"] = Convert.ToInt32(dr.Field<String>("id"));
                    row["is_1c"] = 0;
                    row["is_shop"] = Convert.ToByte(dr.Field<double>("is_shop"));
                    row["category"] = null;
                    row["name"] = dr.Field<String>("name");

                    if (dr.Field<object>("reference") != null)
                    {
                        if (dr.Field<object>("reference").GetType().Name.Equals("String"))
                        {
                            row["reference"] = dr.Field<String>("reference").Trim();
                        }
                        else if (dr.Field<object>("reference").GetType().Name.Equals("DBNull"))
                        {
                            row["reference"] = DBNull.Value;
                        }
                        else
                        {
                            row["reference"] = dr.Field<Double>("reference").ToString();
                        }
                    }
                    row["unity"] = dr.Field<String>("unity");

                    row["manufacturer"] = dr.Field<String>("manufacturer");

                    if (dr.Field<Nullable<Double>>("Закуп. EUR") != null && !(dr.Field<Double>("Закуп. EUR")).Equals(""))
                    {
                        row["retail_price"] = Convert.ToDecimal(dr.Field<Double>("Закуп. EUR"));
                        row["currency"] = "EUR";
                    }
                    else if (dr.Field<Nullable<Double>>("Закуп. руб.") != null && !(dr.Field<Double>("Закуп. руб.")).Equals(""))
                    {
                        row["retail_price"] = Convert.ToDecimal(dr.Field<Double>("Закуп. руб."));
                        row["currency"] = "RUB";
                    }
                    if (dr.Field<Nullable<Double>>("Закуп. USD") != null && !(dr.Field<Double>("Закуп. USD")).Equals(""))
                    {
                        row["retail_price"] = Convert.ToDecimal(dr.Field<Double>("Закуп. USD"));
                        row["currency"] = "USD";
                    }

                    row["e_factor"] = Convert.ToDecimal(dr.Field<Nullable<double>>("e_factor"));
                    row["e_reduction"] = dr.Field<Nullable<Double>>("e_reduction");

                    row["m_factor_1"] = DBNull.Value;                   
                    row["m_factor_2"] = DBNull.Value;                   
                    row["m_factor_3"] = DBNull.Value;
                    
                    row["date_updated"] = DateTime.Now;

                    table.Rows.Add(row);
                }
            }

            return table;
        }
    }
}
