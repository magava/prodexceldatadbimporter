﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelImporterDAL
{
    public interface IExcelImporter
    {
        DataTable GenerateExcelDataTable(DataSet ds, int startRowNumber = 1);
        void SaveExcelDataTableToDB(DataTable table, string connectionString);

    }
}
