﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelImporterDAL
{
    public class ProductInfoExcelImporter : IExcelImporter
    {
        private IDataTableGenerator dataTableGenerator;

        public ProductInfoExcelImporter()
        {
            dataTableGenerator = new ProductsTableGenerator();
        }

        public ProductInfoExcelImporter(IDataTableGenerator dataTableGenerator)
        {
            this.dataTableGenerator = dataTableGenerator;
        }

        public DataTable GenerateExcelDataTable(DataSet ds, int startRowNumber = 1)
        {
            return dataTableGenerator.GenerateDataTable(ds, startRowNumber);
        }

        public void SaveExcelDataTableToDB(DataTable table, string connectionString)
        {
            string tmpTable = "create table #productsTmp(id int, is_1c bit, is_shop bit, category nvarchar(255), name nvarchar(128), reference nvarchar(50), unity nvarchar(20), manufacturer nvarchar(70), retail_price decimal(11, 2), currency char(3), e_factor decimal(9, 6), e_reduction decimal(11, 2),m_factor_1 decimal(9, 6), m_factor_2 decimal(9, 6), m_factor_3 decimal(9, 6), date_updated datetime)";

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                SqlCommand cmd = new SqlCommand(tmpTable, con);
                cmd.ExecuteNonQuery();

                using (SqlBulkCopy bulk = new SqlBulkCopy(con))
                {
                    bulk.DestinationTableName = "#productsTmp";
                    bulk.WriteToServer(table);
                }

                string mergeSql = "set IDENTITY_INSERT products ON " +
                                  "merge into products as Target " +
                                  "using #productsTmp as Source " +
                                  "on " +
                                  "Target.id=Source.id " +
                                  "when matched then " +
                                  "update set " +
                                  "Target.is_1c=Source.is_1c, " +
                                  "Target.is_shop=Source.is_shop, " +
                                  "Target.category=Source.category, " +
                                  "Target.name=Source.name, " +
                                  "Target.reference=Source.reference, " +
                                  "Target.unity=Source.unity, " +
                                  "Target.manufacturer=Source.manufacturer, " +
                                  "Target.retail_price=Source.retail_price, " +
                                  "Target.currency=Source.currency, " +
                                  "Target.e_factor=Source.e_factor, " +
                                  "Target.e_reduction=Source.e_reduction, " +
                                  "Target.m_factor_1=Source.m_factor_1, " +
                                  "Target.m_factor_2=Source.m_factor_2, " +                                
                                  "Target.m_factor_3=Source.m_factor_3, " +                                
                                  "Target.date_updated=Source.date_updated" +
                                  " when not matched and Source.id!=0 then insert (id, is_1c, is_shop, category, name, reference, unity, manufacturer, retail_price, currency, e_factor, e_reduction, m_factor_1, m_factor_2, m_factor_3, date_updated) values (Source.id, Source.is_1c, Source.is_shop, Source.category, Source.name, Source.reference, Source.unity, Source.manufacturer, Source.retail_price, Source.currency, Source.e_factor, Source.e_reduction, Source.m_factor_1, Source.m_factor_2, Source.m_factor_3, Source.date_updated); " +
                                  "set IDENTITY_INSERT products OFF; ";
                
                cmd.CommandText = mergeSql;
                cmd.ExecuteNonQuery();

                cmd.CommandText = "drop table #productsTmp";
                cmd.ExecuteNonQuery();
            }

        }
    }
}
