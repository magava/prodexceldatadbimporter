﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelImporterDAL
{
    public class ReferenceTableGenerator : IDataTableGenerator
    {
        public DataTable GenerateDataTable(DataSet ds, int startRowNumber = 1)
        {
            DataTable table = new DataTable();
            table.Columns.Add("reference", typeof(string));
            table.Columns.Add("name", typeof(string));
            table.Columns.Add("retail_price", typeof(double));
            table.Columns.Add("e_factor", typeof(double));
            table.Columns.Add("date_updated", typeof(DateTime));

            foreach (DataTable dt in ds.Tables)
            {
                for (int i = startRowNumber; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    DataRow row = table.NewRow();

                    if (dr.Field<object>("reference") != null)
                    {
                        if (dr.Field<object>("reference").GetType().Name.Equals("String"))
                        {
                            row["reference"] = dr.Field<String>("reference").Trim();
                        }
                        else if (dr.Field<object>("reference").GetType().Name.Equals("DBNull"))
                        {
                            row["reference"] = DBNull.Value;
                        }
                        else
                        {
                            row["reference"] = dr.Field<Double>(0).ToString();
                        }
                    }
                    row["name"] = dr.Field<String>("name");
                    row["retail_price"] = dr.Field<Double>("retail_price");

                    row["e_factor"] = dr.Field<Nullable<Double>>("e_factor");

                    row["date_updated"] = DateTime.Now;

                    table.Rows.Add(row);
                }
            }

            return table;
        }
    }
}
