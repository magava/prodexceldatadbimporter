﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelImporterDAL
{
    public class ExcelLoader
    {
        public static DataSet GenerateDataSetFromExcel(string fileName)
        {
            FileStream stream = new FileStream(fileName, FileMode.Open);
            IExcelDataReader excelDataReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

            DataSet result = excelDataReader.AsDataSet();

            result = SetColumnNamesAsRowContent(result, 0);
            excelDataReader.Close();
            return result;
        }

        public static DataSet SetColumnNamesAsRowContent(DataSet dataSet, int rowIndex)
        {
            foreach (DataTable dt in dataSet.Tables)
            {
                foreach (DataColumn col in dt.Columns)
                {
                    string cName = dt.Rows[rowIndex][col.ColumnName].ToString();
                    col.ColumnName = cName.Replace("\n", " ");
                }
            }

            return dataSet;
        }
    }
}
