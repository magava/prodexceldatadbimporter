﻿using System;
using System.Data;

namespace ExcelImporterDAL
{
    public class ProductsTableGenerator : IDataTableGenerator
    {
        public DataTable GenerateDataTable(DataSet ds, int startRowNumber = 1)
        {
            DataTable table = new DataTable();
            table.Columns.Add("id", typeof(Int32));

            table.Columns.Add("is_1c", typeof(bool));
            table.Columns.Add("is_shop", typeof(bool));
            table.Columns.Add("category", typeof(string));
            table.Columns.Add("name", typeof(string));
            table.Columns.Add("reference", typeof(string));
            table.Columns.Add("unity", typeof(string));
            table.Columns.Add("manufacturer", typeof(string));
            table.Columns.Add("retail_price", typeof(decimal));
            table.Columns.Add("currency", typeof(string));
            table.Columns.Add("e_factor", typeof(decimal));
            table.Columns.Add("e_reduction", typeof(double));
            
            table.Columns.Add("m_factor_1", typeof(double));            
            table.Columns.Add("m_factor_2", typeof(double));
            table.Columns.Add("m_factor_3", typeof(double));

            table.Columns.Add("date_updated", typeof(DateTime));

            foreach (DataTable dt in ds.Tables)
            {
                for (int i = startRowNumber; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    DataRow row = table.NewRow();

                    row["id"] = Convert.ToInt32(dr.Field<object>("id"));
                    row["is_1c"] = Convert.ToBoolean(dr.Field<Nullable<double>>("is_1c"));
                    row["is_shop"] = Convert.ToBoolean(dr.Field<Nullable<double>>("is_shop"));
                    row["category"] = dr.Field<String>("category");
                    row["name"] = dr.Field<String>("name");

                    row["reference"] = Convert.ToString(dr.Field<Nullable<double>>("reference"));

                    row["unity"] = dr.Field<String>("unity");

                    row["manufacturer"] = dr.Field<String>("manufacturer");

                    row["retail_price"] = Convert.ToDecimal(dr.Field<Nullable<Double>>("retail_price"));
                    row["currency"] = dr.Field<String>("currency");
                    row["e_factor"] = Convert.ToDecimal(dr.Field<Nullable<double>>("e_factor"));

                    string e_reduction = (dr.Field<object>("e_reduction") == null || dr.Field<String>("e_reduction").Equals("NULL")) ? null : dr.Field<String>("e_reduction");
                    row["e_reduction"] = Convert.ToDouble(e_reduction);

                    string m_factor_1 = (dr.Field<object>("m_factor_1") == null || dr.Field<String>("m_factor_1").Equals("NULL")) ? null : dr.Field<String>("m_factor_1");
                    row["m_factor_1"] = Convert.ToDouble(m_factor_1);

                    string m_factor_2 = (dr.Field<object>("m_factor_2") == null || dr.Field<String>("m_factor_2").Equals("NULL")) ? null : dr.Field<String>("m_factor_2");
                    row["m_factor_2"] = Convert.ToDouble(m_factor_2);

                    string m_factor_3 = (dr.Field<object>("m_factor_3") == null || dr.Field<String>("m_factor_3").Equals("NULL")) ? null : dr.Field<String>("m_factor_3");
                    row["m_factor_3"] = Convert.ToDouble(m_factor_3);

                    row["date_updated"] = DateTime.Now;

                    table.Rows.Add(row);
                }
            }

            return table;
        }
    }
}