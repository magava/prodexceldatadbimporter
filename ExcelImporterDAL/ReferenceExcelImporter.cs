﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelImporterDAL
{
    public class ReferenceExcelImporter : IExcelImporter
    {
        private IDataTableGenerator dataTableGenerator;

        public ReferenceExcelImporter()
        {
            dataTableGenerator = new ReferenceTableGenerator();
        }

        public ReferenceExcelImporter(IDataTableGenerator dataTableGenerator)
        {
            this.dataTableGenerator = dataTableGenerator;
        }

        public DataTable GenerateExcelDataTable(DataSet ds, int startRowNumber = 1)
        {
            return dataTableGenerator.GenerateDataTable(ds, startRowNumber);
        }

        public void SaveExcelDataTableToDB(DataTable table, string connectionString)
        {
            string tmpTable = "create table #productsTmp(reference nvarchar(50),name nvarchar(128),retail_price decimal(11, 2), e_factor decimal(9, 6), date_updated datetime)";

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                SqlCommand cmd = new SqlCommand(tmpTable, con);
                cmd.ExecuteNonQuery();

                using (SqlBulkCopy bulk = new SqlBulkCopy(con))
                {
                    bulk.DestinationTableName = "#productsTmp";
                    bulk.WriteToServer(table);
                }

                string mergeSql = "set IDENTITY_INSERT products ON; " +
                                  "merge into products as Target " +
                                  "using #productsTmp as Source " +
                                  "on " +
                                  "Target.reference=Source.reference " +
                                  "when matched then " +
                                  "update set Target.name=Source.name, " +
                                  "Target.retail_price=Source.retail_price, " +
                                  "Target.e_factor=Source.e_factor, " +
                                  "Target.date_updated=Source.date_updated; " +
                                  "set IDENTITY_INSERT products OFF; ";

               
                cmd.CommandText = mergeSql;
                cmd.ExecuteNonQuery();

                cmd.CommandText = "drop table #productsTmp";
                cmd.ExecuteNonQuery();
            }

        }
    }
}
