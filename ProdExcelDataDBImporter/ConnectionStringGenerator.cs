﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProdExcelDataDBImporter
{
    public class ConnectionStringGenerator
    {
        public static string GenerateWinAuth(string serverName, string database)
        {
            return "Data Source=" + serverName + ";Initial Catalog=" + database + ";Integrated Security=True";
        }

        public static string GenerateUserAuth(string serverName, string database, string userId, string password)
        {
            return "Data Source=" + serverName + ";Initial Catalog=" + database + 
                "User ID=" + userId + ";Password=" + password +";Integrated Security=True";
        }
    }
}
