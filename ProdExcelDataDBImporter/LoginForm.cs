﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProdExcelDataDBImporter
{
    public partial class LoginForm : Form
    {
        public static string ConnectionString { get; set; }

        public LoginForm()
        {
            InitializeComponent();
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            rbtnWindowsAuth.Checked = true;
            userLoginFlowPanel.Visible = false;
        }

        private void rbtnWindowsAuth_CheckedChanged(object sender, EventArgs e)
        {
            userLoginFlowPanel.Visible = false;
        }


        private void rbtnSqlServerAuth_CheckedChanged(object sender, EventArgs e)
        {
            userLoginFlowPanel.Visible = rbtnSqlServerAuth.Checked;
        }


        private void btnConnect_Click(object sender, EventArgs e)
        {
            string serverName = txtServerName.Text;
            string databaseName = txtDatabase.Text;
            string userName;
            string password;

            string connString;
            if (!serverName.Equals("") && !databaseName.Equals(""))
            {
                bool isConnectionStringValid = true;

                if (rbtnSqlServerAuth.Checked)
                {
                    userName = txtName.Text;
                    password = txtPassword.Text;
                    connString = ConnectionStringGenerator.GenerateUserAuth(serverName, databaseName, userName, password);
                } else
                {
                    connString = ConnectionStringGenerator.GenerateWinAuth(serverName, databaseName);
                }

                try
                {
                    using (SqlConnection sqlConn = new SqlConnection(connString))
                    {
                        sqlConn.Open();
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Can't connect. Check entered credentials.");
                    isConnectionStringValid = false;
                }



                if (isConnectionStringValid)
                {
                    ConnectionString = connString;

                    MainForm mainForm = new MainForm();
                    mainForm.FormClosed += new FormClosedEventHandler(mainForm_FormClosed);
                    this.Hide();
                    mainForm.Show();
                }

            } 
            else
            {
                MessageBox.Show("Server Name or Database Name can't be empty.");
            }
        }

        private void mainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }
    }
}
