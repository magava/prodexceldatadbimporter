﻿using ExcelImporterDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProdExcelDataDBImporter
{
    public partial class MainForm : Form
    {
        private String ConnectionString { get; set; }

        public MainForm()
        {
            InitializeComponent();
            ConnectionString = LoginForm.ConnectionString;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            btnImport.Enabled = false;
            SetImportesrComboBoxItems();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";

            if (openFileDialog.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }


            if (importersComboBox.Text != null && !importersComboBox.Text.Equals("") && importersComboBox.SelectedIndex != -1)
            {
                DataSet ds = ExcelLoader.GenerateDataSetFromExcel(openFileDialog.FileName);
                OptionComboBoxItem selectedItem = (OptionComboBoxItem)(importersComboBox.Items[importersComboBox.SelectedIndex]);

                DataTable table = selectedItem.Importer.GenerateExcelDataTable(ds, 1);
                selectedItem.Importer.SaveExcelDataTableToDB(table, ConnectionString);
                MessageBox.Show("Operation is finished....");
            }
            else
            {
                MessageBox.Show("Please select one of the options....");
            }
        }

        private void SetImportesrComboBoxItems()
        {
            importersComboBox.Items.Add(new OptionComboBoxItem
            {
                Importer = new ProductInfoExcelImporter(),
                Text = "Update by existing and not empty ID or insert the given not empty ID",
            });

            importersComboBox.Items.Add(new OptionComboBoxItem
            {
                Importer = new EmptyIdExcelImporter(),
                Text = "Update by existing and not empty ID or insert rows with empty ID",
            });

            importersComboBox.Items.Add(new OptionComboBoxItem
            {
                Importer = new ReferenceExcelImporter(),
                Text = "Update by reference",
            });

            importersComboBox.Items.Add(new OptionComboBoxItem
            {
                Importer = new IdFullExcelImporter(),
                Text = "Full Insert and Update by Id",
            });
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            ((OptionComboBoxItem)importersComboBox.Items[0]).Importer = checkBox1.Checked ? new ProductInfoExcelImporter(new ProductsRusOldTableGenerator()) : new ProductInfoExcelImporter();
            ((OptionComboBoxItem)importersComboBox.Items[1]).Importer = checkBox1.Checked ? new EmptyIdExcelImporter(new ProductsRusOldTableGenerator()) : new EmptyIdExcelImporter();
            //((OptionComboBoxItem)importersComboBox.Items[2]).Importer = new ProductInfoExcelImporter(new ProductsTableGenerator());
            ((OptionComboBoxItem)importersComboBox.Items[3]).Importer = checkBox1.Checked ? new IdFullExcelImporter(new ProductsRusOldTableGenerator()) : new IdFullExcelImporter();

        }

        private void importersComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (importersComboBox.Text != null && !importersComboBox.Text.Equals("") && importersComboBox.SelectedIndex != -1)
            {
                btnImport.Enabled = true;
            } else
            {
                btnImport.Enabled = false;
            }
        }
    }
}
