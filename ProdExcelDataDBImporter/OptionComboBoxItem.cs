﻿using ExcelImporterDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProdExcelDataDBImporter
{
    public class OptionComboBoxItem
    {
        public IExcelImporter Importer { get; set; }
        public string Text { get; set; }
       
        public override string ToString()
        {
            return Text;
        }
    }
}
